// Copyright © 2010 Phil Booth
//
// This file is part of VisualSO.
//
// VisualSO is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// VisualSO is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VisualSO.  If not, see <http://www.gnu.org/licenses/>.

using Extensibility;
using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.CommandBars;
using System;
using System.Globalization;
using System.Reflection;
using System.Resources;
using VisualSO.Properties;

namespace VisualSO
{
	public class Connect : IDTExtensibility2, IDTCommandTarget
	{
		public Connect()
		{
		}

		public void OnConnection(object application, ext_ConnectMode connectMode, object addInInst, ref Array custom)
		{
			app = (DTE2)application;
			addIn = (AddIn)addInInst;
			if(connectMode == ext_ConnectMode.ext_cm_UISetup)
			{
				AddMenuItem();
			}
		}

		public void OnDisconnection(ext_DisconnectMode disconnectMode, ref Array custom)
		{
		}

		public void OnAddInsUpdate(ref Array custom)
		{
		}

		public void OnStartupComplete(ref Array custom)
		{
		}

		public void OnBeginShutdown(ref Array custom)
		{
		}
		
		public void QueryStatus(string commandName, vsCommandStatusTextWanted neededText, ref vsCommandStatus status, ref object commandText)
		{
			if(neededText == vsCommandStatusTextWanted.vsCommandStatusTextWantedNone && commandName == commandId)
			{
				if(window.Visible)
					status = (vsCommandStatus)vsCommandStatus.vsCommandStatusSupported;
				else
					status = (vsCommandStatus)vsCommandStatus.vsCommandStatusSupported | vsCommandStatus.vsCommandStatusEnabled;
			}
		}

		public void Exec(string commandName, vsCommandExecOption executeOption, ref object varIn, ref object varOut, ref bool handled)
		{
			handled = false;
			if(executeOption == vsCommandExecOption.vsCommandExecOptionDoDefault && commandName == commandId)
			{
				CreateWindow();
				handled = true;
			}
		}

		private void AddMenuItem()
		{
			try
			{
				CommandBarPopup menuPopup = (CommandBarPopup)((Microsoft.VisualStudio.CommandBars.CommandBars)app.CommandBars)["MenuBar"].Controls[localisedMenuName];
				if(menuPopup == null)
					return;

				object[] contextGUIDS = new object[] { };

				Command command = ((Commands2)app.Commands).AddNamedCommand2(addIn, Controller.addinName, buttonCaption, buttonTooltip, buttonIconIsFromMSOffice, buttonIconId, ref contextGUIDS, (int)vsCommandStatus.vsCommandStatusSupported + (int)vsCommandStatus.vsCommandStatusEnabled, (int)vsCommandStyle.vsCommandStylePictAndText, vsCommandControlType.vsCommandControlTypeButton);
				if(command == null)
					return;

				command.AddControl(menuPopup.CommandBar, 1);
			}
			catch(ArgumentException)
			{
				// A menu item with that name already exists
			}
		}

		private string localisedMenuName
		{
			get
			{
				try
				{
					ResourceManager resourceManager = new ResourceManager(resourceName, Assembly.GetExecutingAssembly());
					CultureInfo cultureInfo = new CultureInfo(app.LocaleID);
					return resourceManager.GetString(String.Concat(cultureInfo.TwoLetterISOLanguageName, menuName));
				}
				catch
				{
				}

				return menuName;
			}
		}

		private void CreateWindow()
		{
			object control = null;
			window = ((Windows2)app.Windows).CreateToolWindow2(addIn, Controller.controlLibraryPath, Controller.controlName, windowCaption, windowGUID, ref control);
			window.Visible = true;
			InitialiseController(control);
		}

		private void InitialiseController(object renderer)
		{
			controller = new Controller(renderer);
		}

		private readonly static string className = "VisualSO.Connect";
		private readonly static string commandId = className + "." + Controller.addinName;
		private readonly static string buttonCaption = "Visual SO";
		private readonly static string buttonTooltip = "Open Visual SO";
		private readonly static bool buttonIconIsFromMSOffice = false;
		private readonly static int buttonIconId = 50;
		private readonly static string resourceName = "VisualSO.CommandBar";
		private readonly static string menuName = "Tools";
		private readonly static string windowCaption = "Visual SO";
		private readonly static string windowGUID = "{00DFACB1-2DDE-4b88-89D4-CA975FE7314C}";

		private DTE2 app;
		private AddIn addIn;
		private Window window;
		private Controller controller;
	}
}
