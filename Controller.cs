// Copyright � 2010 Phil Booth
//
// This file is part of VisualSO.
//
// VisualSO is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// VisualSO is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VisualSO.  If not, see <http://www.gnu.org/licenses/>.

using PB;
using Soapi;
using Soapi.Domain;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using VisualSO.Properties;

// SO DOCS: http://api.stackoverflow.com/1.0/help
// SO API KEY: wWvFk3Zewk6XvFChfXmpfw
// SO APP NAME: Visual SO
// Soapi.CS DOCS: http://stackapps.com/questions/386/soapi-cs-a-fully-relational-fluent-net-stack-exchange-api-client-library

// TODO: I18N

namespace VisualSO
{
	public class Controller
	{
		public Controller(object view)
		{
			InitaliseModel();
			InitialiseView(view);
			RenderView();
		}

		public void Open(string url)
		{
			try
			{
				Process.Start(url);
			}
			catch(Win32Exception)
			{
			}
			catch(FileNotFoundException)
			{
			}
		}

		public static string controlLibraryPath
		{
			get { return addinDirectory + Path.DirectorySeparatorChar + controlLibraryName; }
		}

		private static string addinDirectory
		{
			get { return Path.GetDirectoryName(addinPath); }
		}

		private static string addinPath
		{
			get { return Uri.UnescapeDataString((new UriBuilder(Assembly.GetExecutingAssembly().CodeBase).Path)); }
		}

		private void InitaliseModel()
		{
			model = new ApiContext(apiKey)
				.Options
					.UserAgent(userAgent)
					.LazyLoadingEnabled(true)
					.EagerLoadingEnabled(true)
				.Context
					.Initialize(false)
					.Official
						.StackOverflow;
		}

		private string userAgent
		{
			get { return String.Format("{0}/{1} (.NET {2}) {3}/{4}", addinName, addinVersion, runtimeVersion, modelName, modelVersion); }
		}

		public static string addinName
		{
			get { return Assembly.GetExecutingAssembly().GetName().Name; }
		}

		private static string addinVersion
		{
			get { return GetAssemblyVersion(addinPath); }
		}

		private static string GetAssemblyVersion(string assemblyPath)
		{
			return GetAssemblyName(assemblyPath).Version.ToString();
		}

		private static AssemblyName GetAssemblyName(string assemblyPath)
		{
			return AssemblyName.GetAssemblyName(assemblyPath);
		}

		private static string runtimeVersion
		{
			get { return Assembly.GetExecutingAssembly().ImageRuntimeVersion; }
		}

		private static string modelName
		{
			get { return GetAssemblyName(modelLibraryPath).Name; }
		}

		private static string modelLibraryPath
		{
			get { return addinDirectory + Path.DirectorySeparatorChar + modelLibraryName; }
		}

		private static string modelVersion
		{
			get { return GetAssemblyVersion(modelLibraryPath); }
		}

		private void InitialiseView(object renderer)
		{
			view = (HTMLRenderer)renderer;
			view.SetCallback(this);
		}

		private void RenderView()
		{
			string body = "";
			foreach(Question question in model.Questions)
				body += GetQuestionMarkup(question.Title, question.QuestionAnswersUrl, question.Score);
			view.RenderMarkup(String.Format(viewTemplate, body));
		}

		private string GetQuestionMarkup(string question, string url, int score)
		{
			return String.Format("<div class=\"clickable\" onclick=\"VisualSO.openExternal('{0}');\">{1} [{2}]</div>", model.SiteUrl + url, question, score);
		}

		private static string viewTemplate
		{
			get { return String.Format(Resources.View_html, filePrefix + addinDirectory + Path.DirectorySeparatorChar); }
		}

		public readonly static string controlName = "PB.HTMLRenderer";

		private readonly static string controlLibraryName = "PBCtrl.dll";
		private readonly static string filePrefix = "file:///";
		private readonly static string apiKey = "wWvFk3Zewk6XvFChfXmpfw";
		private readonly static string modelLibraryName = "Soapi.dll";

		private HTMLRenderer view;
		private Site model;
	}
}
